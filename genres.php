<?php
// this file is the same as songs-for-genre-mysqli.php but uses PDO
$host = 'itp460.usc.edu';
$database_name = 'music';
$username = 'student';
$password = 'ttrojan';
$pdo = new PDO("mysql:host=$host;dbname=$database_name", $username, $password);

$genre = $_GET['genre'];
// $genre = 'Hip Hop';

$sql = "
  SELECT title, artist_name
  FROM songs, genres, artists
  WHERE genres.id = songs.genre_id
  AND artists.id = songs.artist_id
  AND genres.genre = ?
";
// $sql = "
//   SELECT title, artist_name
//   FROM songs
//   INNER JOIN genres
//   ON genres.id = songs.genre_id
//   INNER JOIN artists
//   ON artists.id = songs.artist_id
//   WHERE genres.genre = ?
// ";

$statement = $pdo->prepare($sql);
$statement->bindParam(1, $genre);
$statement->execute();
$songs = $statement->fetchAll(PDO::FETCH_OBJ);
// var_dump($songs);
?>

<ul>
  <?php foreach ($songs as $song) : ?>
    <li>
      <?php echo $song->title ?> by <?php echo $song->artist_name ?>
    </li>
  <?php endforeach; ?>
</ul>
